/*  Edit this string to change the inputs */
var testInput = "\n5 3\n1 1 E\nRFRFRFRF\n\n3 2 N\nFRRFLLFFRRFLL\n\n0 3 W\nLLFFFLFLFL\n";
/* in this function, `this` will be passed in as the point to be found, and the `point` will be the current element of the array*/
function findPoint(point) {
    if (point.x === this.x && point.y === this.y) {
        return point;
    }
}
var LOST_ERROR = 'LOST';
var VALID_INSTRUCTION_CHARS_REGEX = /[A-Z0-9\ ]+/;
var DIRECTIONS = ["N", "E", "S", "W"];
var Grid = /** @class */ (function () {
    function Grid(gridInit) {
        var coords = gridInit.split(" ");
        this.losts = new Array();
        this.max = { x: parseInt(coords[0]), y: parseInt(coords[1]) };
    }
    Grid.prototype.isIn = function (pos) {
        return pos.x <= this.max.x && pos.y <= this.max.y && pos.x >= 0 && pos.y >= 0;
    };
    /* Straightforward -- checks if current position is known to possibly be Losting*/
    Grid.prototype.couldLost = function (pos) {
        return this.losts.find(findPoint, pos);
    };
    /*
    Just adds a given point to the tracked list of 'Losts',
    if it is not in there already, and if it is on an edge of the grid (you can't fall off the middle of a map)
    */
    Grid.prototype.addLost = function (pos) {
        if (!this.losts.find(findPoint, pos)
            && (pos.x == this.max.x || pos.y == this.max.y || pos.x == 0 || pos.y == 0)) {
            this.losts.push(pos);
        }
    };
    /*
    Take a start and a direction to move, and  return resulting point or LOST_ERROR
    Does not add "Losts" because that is a robot's job and if we let our code take robot's jobs then robots are more likely to take our jobs.
    */
    Grid.prototype.calculateMove = function (pos, direction) {
        var newPos;
        // this can maybe have some magic by changing the type of Dir to add to x/y coords properly
        switch (direction) {
            case 'N': {
                newPos = { x: pos.x, y: pos.y + 1 };
                break;
            }
            case 'S': {
                newPos = { x: pos.x, y: pos.y - 1 };
                break;
            }
            case 'E': {
                newPos = { x: pos.x + 1, y: pos.y };
                break;
            }
            case 'W': {
                newPos = { x: pos.x - 1, y: pos.y };
                break;
            }
        }
        if (this.isIn(newPos)) {
            return newPos;
        }
        else {
            return this.couldLost(pos) ? pos : LOST_ERROR;
        }
    };
    return Grid;
}());
var Bot = /** @class */ (function () {
    function Bot(botInit) {
        var initVals = botInit.split(" ");
        this.position = { x: parseInt(initVals[0]), y: parseInt(initVals[1]) };
        this.direction = initVals[2];
    }
    Bot.prototype.setGrid = function (grid) {
        this.grid = grid;
    };
    Bot.prototype.getGrid = function () {
        return this.grid;
    };
    Bot.prototype.setFinalStatus = function (error) {
        this.finalStatus = this.position.x + " " + this.position.y + " " + this.direction;
        if (error) {
            this.finalStatus += " " + error;
        }
    };
    Bot.prototype.hasFinalStatus = function () {
        // pretty sure there's an es6 shortcut to force
        // evaluating the property to a boolean but this works 
        return this.finalStatus ? true : false;
    };
    Bot.prototype.command = function (i) {
        var dirIndex = DIRECTIONS.indexOf(this.direction);
        switch (i) {
            case "L": {
                dirIndex = dirIndex === 0 ? DIRECTIONS.length - 1 : (dirIndex - 1) % DIRECTIONS.length;
                return this.direction = DIRECTIONS[dirIndex];
            }
            case "R": {
                dirIndex = (dirIndex + 1) % DIRECTIONS.length;
                return this.direction = DIRECTIONS[dirIndex];
            }
            case "F": {
                var res = this.grid.calculateMove(this.position, this.direction);
                if (res === LOST_ERROR) {
                    this.setFinalStatus(res);
                    this.grid.addLost(this.position);
                }
                else {
                    this.position = res;
                }
                return this.hasFinalStatus();
                break;
            }
            /* sample for adding in a new "Backward" B instr by reparsing as existing instr.
            case "B": {
              const newInstr:BotInstruction[] = ["R","R","F"];
              newInstr.forEach(inst => {
                this.command(inst);
              });
            (doesnt return final command's result but could be easily modified to do so.)
            }*/
        }
    };
    return Bot;
}());
function parsingLoop(theInput) {
    // split instructions by new lines, and remove any invalid lines
    var input = theInput
        .split('\n')
        .filter(function (instr) { return /[A-Z0-9\ ]+/.test(instr); });
    // take first one to make the grid
    var gridMax = input.splice(0, 1)[0];
    var theGrid = new Grid(gridMax);
    if (input.length % 2 != 0) {
        console.log("I parsed an uneven amount of instructions for the bots. Each bot needs initial values, and a string of instructions.");
        return false;
    }
    // go through each pair of instructions and bot it up
    while (input.length > 0) {
        var bot = new Bot(input.shift());
        bot.setGrid(theGrid);
        var instructions = input.shift().split("");
        while (instructions.length > 0 && !bot.hasFinalStatus()) {
            var result = bot.command(instructions.shift());
        }
        if (!bot.hasFinalStatus()) {
            bot.setFinalStatus("");
        }
        console.log(bot.finalStatus);
    }
}
parsingLoop(testInput);
/* TESTING for Grid and Bot */
// const grid1 = new Grid( "5 3");
// console.log('The following should be: true, false, true');
// console.log(grid1.isIn({ x: 5, y: 3 }));
// console.log(grid1.isIn({ x: -1, y: 3 }));
// console.log(grid1.isIn({ x: 3, y: 2 }));
// console.log('The following should read {3,3}, {3,2}, LOST');
// console.log(grid1.calculateMove({ x: 3, y: 2 }, "N"));
// console.log(grid1.calculateMove({ x: 3, y: 3 }, "S"));
// console.log(grid1.calculateMove({ x: 3, y: 3 }, "N"));
// console.log('Now we should see {3,3} because we added a lost point');
// grid1.addLost({ x: 3, y: 3 });
// console.log(grid1.calculateMove({ x: 3, y: 3 }, "N"));
// console.log('Create a Bot, should have vector (1,1) East');
// const bot1 = new Bot("1 1 E");
// console.log(bot1.position);
// console.log(bot1.direction);
// console.log("rotate the bot Rightways 4x should go S, W, N, E");
// console.log(bot1.command("R"));
// console.log(bot1.command("R"));
// console.log(bot1.command("R"));
// console.log(bot1.command("R"));
// console.log("rotate the bot leftways 4x should go N, W, S, E");
// console.log(bot1.command("L")); 
// console.log(bot1.command("L"));
// console.log(bot1.command("L"));
// console.log(bot1.command("L"));
