# RB

## Instructions:
The important code is in `marsbots.ts`

Run with node via `node marsbots.js`

To recompile (not necessary, but you'll probably want to change the inputs or run the tests at the bottom of the file) instructions are as follows:

 If you haven't already got typescript configured you will need to install typescript:
`npm install -g typescript`

Then you can compile the .ts file using
`tsc marsbots.ts`


## Changing the input parameters
I didn't spend time writing any parser (or frontend). You can edit the `const testInput` at the top which accepts a line-separated list of parameters that matches the given input.

I did not defend too heavily against malformed inputs, more on that later.


## Code Structure:
I wrote a Grid and a Bot class which do most of the work. The Grid is able to determine if a point is inside or outside the grid. The function calculateMove accepts a point and direction, and returns the result for a move 1 step in the given direction, or a "lost error" if it would be off grid.
The Bot class stores a current position and direction, and can run command() to rotate left, right, and move forward. It can easily be extended with new commands  -- I included an example of "Backward" movement by chaining together two turns and a forward movement.


## Typescript
I chose to use typescript because I thought it would make it a bit easier to enforce the validity of the input/instructions.

However, I didn't factor in that it has been a while since I've used typescript, and I haven't used it in this format before -- so it added a bit of time to my development process. I am happy with the result, mostly, and have added comments where I felt I could have better utilized typescript's features or could improve things.


## Addons / improvements
- Because of the (perhaps foolish) choice to use a language I'm not as familiar with, I have left the code in a single file. 
	- It's small enough that I think this is not entirely inappropriate.
	- However, for a larger scale project or for a more elegant solution, I would definitely move the classes into separate files, put tests into a standalone file, etc.
- I did not put a front-end on this -- it would be fairly simple to display a Grid as a React component with square divs for each cell, and then Bot Components could traverse the grid, with some css animation and timing.
- There's an optimization in there somewhere for converting L/R (relative directional instructions) into cardinal directions (NESW) without switch statements, and I have partially done that by including a const array of directions which can be traversed, but I think there's probably a better TypeScript way to enforce the instructions and the directions. 
